﻿using DbProductos_Autogermana.DataAccess.Interfaces;
using DbProductos_Autogermana.Entities;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace DbProductos_Autogermana.Controllers
{
    [SwaggerTag("Servicios de Entidad Categoria")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : Controller
    {
        public ICategoriaRepository _categoriaRepository;

        public CategoriaController(ICategoriaRepository categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }

        /// <summary>
        /// POST para Crear una nueva categoría
        /// </summary>
        /// <param name="categoria">Datos de la categoría</param>
        /// <returns>ID de categoría creada</returns>
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create(Categoria categoria)
        {
            try
            {
                int categoriaId = _categoriaRepository.Create(categoria);
                return Ok(categoriaId);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// GET para obener datos de una categoría por su nombre
        /// </summary>
        /// <param name="nombre">Nombre de la categoría</param>
        /// <returns>Datos de la categoría</returns>
        [HttpGet]
        [Route("GetByNombre/{nombre}")]
        public async Task<IActionResult> GetByNombre(string nombre)
        {
            try
            {
                IEnumerable<Categoria> categorias = _categoriaRepository.Get(x => x.Nombre.Trim() == nombre.Trim());
                return Ok(categorias);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// GET para obtener lista con todas las categorías creadas
        /// </summary>
        /// <returns>Lista de categorías</returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                IEnumerable<Categoria> categorias = _categoriaRepository.GetAll();
                return Ok(categorias);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// GET para obtener categoría por su Id
        /// </summary>
        /// <param name="categoriaId">ID de categoría</param>
        /// <returns>Datos de categoría</returns>
        [HttpGet]
        [Route("FindById/{categoriaId}")]
        public async Task<IActionResult> FindById(int categoriaId)
        {
            try
            {
                Categoria categoria = _categoriaRepository.Find(categoriaId);
                return Ok(categoria);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// POST para actualizar datos de una categoría, el ID debe ser el mismo
        /// </summary>
        /// <param name="categoria">Datos de la categoría</param>
        /// <returns>Indicador de actualización exitosa</returns>
        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update(Categoria categoria)
        {
            try
            {
                _categoriaRepository.Update(categoria);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// POST para eliminar registro de una categoría, el ID debe ser el mismo
        /// </summary>
        /// <param name="categoria">Datos de la categoría</param>
        /// <returns>Indicador de eliminación exitosa</returns>
        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete(Categoria categoria)
        {
            try
            {
                _categoriaRepository.Delete(categoria);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
