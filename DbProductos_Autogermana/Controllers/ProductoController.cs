﻿using DbProductos_Autogermana.DataAccess.Interfaces;
using DbProductos_Autogermana.Entities;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace DbProductos_Autogermana.Controllers
{
    [SwaggerTag("Servicios de Entidad Producto")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : Controller
    {
        public IProductoRepository _productoRepository;

        public ProductoController(IProductoRepository productoRepository)
        {
            _productoRepository = productoRepository;
        }

        /// <summary>
        /// POST para Crear un nuevo producto
        /// </summary>
        /// <param name="producto">Datos del producto</param>
        /// <returns>ID del producto creado</returns>
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create(Producto producto)
        {
            try
            {
                int productoId = _productoRepository.Create(producto);
                return Ok(productoId);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// GET para obener datos de un producto por su nombre
        /// </summary>
        /// <param name="nombre">Nombre del producto</param>
        /// <returns>Datos del producto</returns>
        [HttpGet]
        [Route("GetByNombre/{nombre}")]
        public async Task<IActionResult> GetByNombre(string nombre)
        {
            try
            {
                IEnumerable<Producto> productos = _productoRepository.Get(x => x.Nombre == nombre);
                return Ok(productos);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// GET para obtener lista con todos los productos creados
        /// </summary>
        /// <returns>Lista de productos</returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                IEnumerable<Producto> productos = _productoRepository.GetAll();
                return Ok(productos);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// GET para obtener producto por su Id
        /// </summary>
        /// <param name="productoId">ID de producto</param>
        /// <returns>Datos de producto</returns>
        [HttpGet]
        [Route("FindById/{productoId}")]
        public async Task<IActionResult> FindById(int productoId)
        {
            try
            {
                Producto producto = _productoRepository.Find(productoId);
                return Ok(producto);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// POST para actualizar datos de un producto, el ID debe ser el mismo
        /// </summary>
        /// <param name="producto">Datos del producto</param>
        /// <returns>Indicador de actualización exitosa</returns>
        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update(Producto producto)
        {
            try
            {
                _productoRepository.Update(producto);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// POST para eliminar registro de un producto, el ID debe ser el mismo
        /// </summary>
        /// <param name="producto">Datos del producto</param>
        /// <returns>Indicador de eliminación exitosa</returns>
        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete(Producto producto)
        {
            try
            {
                _productoRepository.Delete(producto);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
