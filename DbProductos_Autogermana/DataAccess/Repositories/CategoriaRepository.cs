﻿using DbProductos_Autogermana.DataAccess.Interfaces;
using DbProductos_Autogermana.Entities;

namespace DbProductos_Autogermana.DataAccess.Repositories
{
    public class CategoriaRepository : BaseRepository<Categoria>, ICategoriaRepository
    {
        public CategoriaRepository(DbProductContext context) : base(context)
        {
        }

        public override int Create(Categoria entity)
        {
            Context.Categoria.Add(entity);
            Context.SaveChanges();
            return entity.IdCategoria;
        }

        public override Categoria Find(int id)
        {
            return Context.Categoria.FirstOrDefault(x => x.IdCategoria == id);
        }
    }
}
