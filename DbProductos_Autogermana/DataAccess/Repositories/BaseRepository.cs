﻿using System.Linq.Expressions;
using DbProductos_Autogermana.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DbProductos_Autogermana.DataAccess.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected DbProductContext _context = null;
        private DbSet<T> _entity = null;
        public BaseRepository(DbProductContext context)
        {
            this._context = context;
            this._entity = context.Set<T>();
        }

        public DbProductContext Context => _context;
        public DbProductContext GetContext()
        {
            return this._context;
        }

        public abstract int Create(T entity);
        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return _entity.Where(predicate)
                .AsNoTracking()
                .ToList();
        }
        public IEnumerable<T> GetAll()
        {
            return _entity
                .AsNoTracking()
                .ToList();
        }
        public abstract T Find(int id);
        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }
        public void Delete(T entity)
        {
            _entity.Remove(entity);
            Context.SaveChanges();
        }
    }
}
