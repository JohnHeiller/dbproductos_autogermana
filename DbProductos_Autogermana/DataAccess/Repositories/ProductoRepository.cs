﻿using DbProductos_Autogermana.DataAccess.Interfaces;
using DbProductos_Autogermana.Entities;

namespace DbProductos_Autogermana.DataAccess.Repositories
{
    public class ProductoRepository : BaseRepository<Producto>, IProductoRepository
    {
        public ProductoRepository(DbProductContext context) : base(context)
        {
        }

        public override int Create(Producto entity)
        {
            Context.Producto.Add(entity);
            Context.SaveChanges();
            return entity.IdProducto;
        }

        public override Producto Find(int id)
        {
            return Context.Producto.FirstOrDefault(x => x.IdProducto == id);
        }
    }
}
