﻿using System.Linq.Expressions;

namespace DbProductos_Autogermana.DataAccess.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        DbProductContext GetContext();

        int Create(T entity);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();
        T Find(int id);
        void Update(T entity);
        void Delete(T entity);
    }
}
