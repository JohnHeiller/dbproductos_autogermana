﻿using DbProductos_Autogermana.Entities;

namespace DbProductos_Autogermana.DataAccess.Interfaces
{
    public interface IProductoRepository : IBaseRepository<Producto>
    {
    }
}
