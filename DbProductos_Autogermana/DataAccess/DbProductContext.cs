﻿using DbProductos_Autogermana.Entities;
using Microsoft.EntityFrameworkCore;

namespace DbProductos_Autogermana.DataAccess
{
    public class DbProductContext : DbContext
    {
        public IConfiguration Configuration { get; }

        public DbProductContext(DbContextOptions<DbProductContext> options, IConfiguration configuration) : base(options)
        {
            Configuration = configuration;
        }

        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<Producto> Producto { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Producto>().Property(o => o.Precio_Venta)
                .HasPrecision(11, 2)
                .IsRequired();
            modelBuilder.Entity<Producto>().HasIndex(o => o.Nombre)
                .IsUnique();
            modelBuilder.Entity<Producto>().Property(o => o.Estado)
                .HasDefaultValue(true);

            modelBuilder.Entity<Categoria>().HasIndex(o => o.Nombre)
                .IsUnique();
            modelBuilder.Entity<Categoria>().Property(o => o.Estado)
                .HasDefaultValue(true);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                var connectionString = Configuration.GetConnectionString("DefaultConnection");
                options.UseSqlServer(connectionString);
            }
        }
    }
}
