﻿using System.ComponentModel.DataAnnotations;

namespace DbProductos_Autogermana.Entities
{
    public class Categoria
    {
        [Key]
        public int IdCategoria { get; set; }
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }
        [StringLength(255)]
        public string? Descripcion { get; set; }
        public bool Estado { get; set; }
    }
}
