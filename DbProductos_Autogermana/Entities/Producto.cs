﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DbProductos_Autogermana.Entities
{
    public class Producto
    {
        [Key]
        public int IdProducto { get; set; }
        public int IdCategoria { get; set; }
        [StringLength(64)]
        public string? Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
        public decimal Precio_Venta { get; set; }
        public int Stock { get; set; }
        [StringLength(255)]
        public string? Descripcion { get; set; }
        public byte[]? Imagen { get; set; }
        public bool Estado { get; set; }
        [ForeignKey("IdCategoria")]
        public virtual Categoria? Categoria { get; set; }
    }
}
