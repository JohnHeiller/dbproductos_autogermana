# DbProductos Autogermana

DbProductos_Autogermana es un proyecto creado como ejercicio técnico sobre el manejo de microservicios en .Net Core. Expone servicios tipo CRUD para dos entidades propuestas (Categoría y producto).

## Desarrollo

1. Se creó un proyecto tipo Web API en framework .NET 6.0, agrupando las clases en "capas" para ordenar métodos: 
   * *Controllers*: Clases con métodos de servicios expuestos,
   * *DataAccess*: Clases con métodos de manejo repositorio CRUD por cada entidad (tabla en BD), usando EntityFramework Core v7; aquí también se encuentra el DbContext,
   * *Entities*: Clases con propiedades que reflejan las tablas de la BD.

2. Se usó **swagger** para documentar y exponer los servicios en algo similar a un *Portal para desarrolladores*, el proyecto se lanzará y mostrará este como primera pantalla:  `https://localhost:[port]/swagger/index.html`

3. Se implementó el EntityFramework para permitir la migración de la base de datos desde el proyecto (Code first).

4. Se usó *Inyección de Dependencias* entre controladores y repositorios, y se independizaron los métodos por cada servicio, así como la administración de acceso a la BD desde la aplicación desde clases individuales (Repository) por cada entidad. Esto como base de un escalamiento horizontal futuro, facilitando la implementación posterior de balanceadores para BD o herramientas de manejo de Caché para reducir carga por transacciones a la BD.

5. Se implementó el uso de *interfaces* por cada clase de las "capas" bajas del proyecto, agregando seguridad al sistema.

6. Se agregó un control de errores o excepciones, facilitando el uso posterior de *logger* para registro de actividad del API y, presentación de mensajes controlados al usuario final.